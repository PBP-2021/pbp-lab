# Lab 7: Flutter Input & Form

CSGE602022 - Platform-Based Programming (Pemrograman Berbasis Platform) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2021/2022

---

## Learning Objectives

After completing this tutorial, students are expected to be able to:

- Understand the utilization of Input & Form using Flutter to build the
  interface structure of a mobile application
- Implement a simple mobile app on top of the Flutter framework

## Tasks

You are tasked to create a new app in this project named `lab_7`, which is a
project on top of the Flutter framework that displays a page that was
previously made for the web platform. Make sure that the page contains:

1. A widget that shows a form input element, e.g. `TextFormField`, `IconButton`
2. An implementation of `onChanged` or `onClick` to do a simple action, e.g.
   showing a text content, printing an input to the console screen, etc.

## Checklist

### Running Sample Program
1. [ ] Go to directory `lab_7` in root directory (`pbp-lab`).
2. [ ] Run `flutter create .` to initiate flutter in your `lab_7` directory. If
   you would like to try `lab_7` boilerplate, just run `flutter run`.

### Implement my own Input page
3. [ ] Try to reuse components and widget from boilerplate and modify it as
   required.
4. [ ] Try the application that you have built in this lab using Web Browser /
   Mobile Devices / Simulator, just run `flutter run`.

## References

1. https://belajarflutter.com/tutorial-cara-membuat-form-di-flutter-lengkap/
2. Official Flutter Docs: https://flutter.io/docs/
